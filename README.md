## Pasos
1. En la capeta root o jubert-goya dirigirse a la carpeta "practico":
```
cd practico
```
2. En la carpeta practico, tener en ejecicion docker y escribir:
```
docker-compose up
```
Esperar hasta que los contenedores se creen.

3. Luego dirigirse a localhost:3000 para visualizar el front


- El servidor back end se encuentra en localhost:1000
- Se guardo un archivo json con las solicitudes que se pueden importar en postman
- El usuario admin es username: "admin1", constrasena: "1234"

