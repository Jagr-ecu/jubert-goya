function checkNumeroMayorA10(numero) {
    switch (numero){
        case 10:
            return 'A';
        case 11:
            return 'B';
        case 12:
            return 'C';
        case 13:
            return 'D';
        case 14:
            return 'E';
        case 15:
            return 'F';
        default:
            return numero;
    }
}

function convertToBase(numeroDecimal, base) {
   const resultado = [];
    let division = numeroDecimal;

    //solo puse el limite para base 16
    if(base > 16) return 'base mayor de 16, no fue posible calcular'

    //ejecuta hasta que los resultados de la division del numero sea 0
    while(division > 0){
        //se calcula el residuo de la division
        const residuo = division % base;
        //se lo divide y se obtiene el numero entero
        division = Math.floor(division / base);
        //se agrega el residuo a un arreglo en donde se encontraran los demas residuos
        resultado.push(checkNumeroMayorA10(residuo));
    }

    //se retorna el arreglo de los residios unidos en un numero
    return resultado.reverse().join('')
}

const resultado = convertToBase(20, 8)
console.log('El resultado es '+resultado);
