//sirve para generar un numero random con un rango
function randomIntFromInterval(min, max) {
   return Math.floor(Math.random() * (max - min + 1) + min);
}

//genera un string
function generador() {
   var resultado = "";
   //string que sirve como base para escoger los caracteres de manera aleatoria 
   var letras =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-*/&%$#!=()@-*/&%$#!=()@-*/&%$#!=()@";
   //genera un numero random del 8 al 15
      var tamañoString = randomIntFromInterval(8, 15);

      //escoge caracteres de manera aleatoria de "letras"
   for (var i = 0; i < tamañoString; i++) {
      resultado += letras.charAt(Math.floor(Math.random() * letras.length));
   }
   //retorna clave
   return resultado;
}

function generarClave() {
   //genera clave
   var clave = generador();
   var i = 0;
   var letra = "";

   //flags para verficiar seguridad de clave
   let hayNumero = false;
   let hayMayuscula = false;
   let hayMinuscula = false;
   let hayCaracterEsp = false;

   //por cada letra en la clave se verifica si es numero, mayuscula, etc...
   //y si cumple se activa uno de los flags anteriores por letras
   while (i <= clave.length) {
      letra = clave.charAt(i);
      if (!isNaN(letra * 1)) {
         hayNumero = true;
      }
      if (letra == letra.toUpperCase()) {
         hayMayuscula = true;
      }
      if (letra == letra.toLowerCase()) {
         hayMinuscula = true;
      }
      if (
         letra == "*" ||
         letra == "/" ||
         letra == "&" ||
         letra == "%" ||
         letra == "$" ||
         letra == "#" ||
         letra == "!" ||
         letra == "=" ||
         letra == "(" ||
         letra == ")" ||
         letra == "@"
      ) {
         hayCaracterEsp = true;
      }

      i++;
   }
   
   //al final se verifican los flag si cumplio con lo requerido
   //en caso de que no cumpla se vuelve a ejecutar la funcion con una clave nueva
   if (hayCaracterEsp && hayMayuscula && hayNumero && hayMinuscula){
    console.log(clave);
    return clave
   } else {
        generarClave()
   }
}

generarClave()

