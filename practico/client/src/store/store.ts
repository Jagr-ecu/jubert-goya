import { useContext, createContext } from "react";
import GeneralStore from "./general-store";
import AuthStore from "./auth-store";
import ModalStore from './modal-store';
import CartStore from './cart-store';

interface Store {
    authStore: AuthStore;
    generalStore: GeneralStore;
    modalStore: ModalStore;
    cartStore: CartStore
}

export const store: Store = {
    authStore: new AuthStore(),
    generalStore: new GeneralStore(),
    modalStore: new ModalStore(),
    cartStore: new CartStore()
}

export const StoreContext = createContext(store);

export function useStore() {
    return useContext(StoreContext);
}