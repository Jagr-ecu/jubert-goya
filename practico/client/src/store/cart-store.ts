import { makeAutoObservable, runInAction } from "mobx";
import { ProductoInterface } from "../interfaces/Producto";

export interface Cart {
   id: number;
   title: string;
   price: number;
   description: string;
   category: string;
   image: string;
   fechaCreacion: string;
   cantidad: number;
}

export default class CartStore {
   cartItems = new Map<string, Cart>();
   cartContador: number = 0;
   cartTotal: number = 0;

   constructor() {
      makeAutoObservable(this);
   }

   private getItem = (id: number) => {
      return this.cartItems.get(id.toString());
   };

   obtenerCantidad = (idProducto: number) => {
    return this.getItem(idProducto)?.cantidad
   }

   añadirItem = (productoAñadir: Cart) => {
      if (
         !this.getItem(productoAñadir.id) ||
         this.getItem(productoAñadir.id)?.cantidad === 0
      ) {
         this.cartItems.set(productoAñadir.id.toString(), {
            ...productoAñadir,
            cantidad: 1,
         });
      } else {
          const itemCantidad = this.getItem(productoAñadir.id)?.cantidad! + 1;
          console.log(itemCantidad);
          
    
          this.cartItems.set(productoAñadir.id.toString(), {
             ...productoAñadir,
             cantidad: itemCantidad,
          });
      }
   };

   quitarItem = (productoARemover: Cart) => {
    if (
        this.getItem(productoARemover.id)?.cantidad === 1
     ) {
        this.cartItems.set(productoARemover.id.toString(), {
           ...productoARemover,
           cantidad: 0,
        });
     } else {
         const itemCantidad = this.getItem(productoARemover.id)?.cantidad! - 1;
         console.log(itemCantidad);
         
         this.cartItems.set(productoARemover.id.toString(), {
            ...productoARemover,
            cantidad: itemCantidad,
         });
     }
   };

   limpiarCart = (productoARemover: ProductoInterface) => {
    this.cartItems.delete(productoARemover.id.toString())
   };

   get getCart() {
    return Array.from(this.cartItems.values())
   }

   
}
