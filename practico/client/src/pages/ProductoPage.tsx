import { Box, Container, Grid, Paper, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import apiUrls from "../api/axios-agent";
import { ProductoInterface } from "../interfaces/Producto";

const ProductoPage = () => {
   const [producto, setProducto] = useState<ProductoInterface>({} as ProductoInterface);
   const { id } = useParams<{ id: string }>();

   useEffect(() => {
      fetchProducto();
   }, []);

   const fetchProducto = async () => {
      const productoApi = await apiUrls.Producto.getProducto(id!);
      
      setProducto(productoApi);
   };

   return (
      <Container>
         <Paper
            sx={{
               position: "relative",
               backgroundColor: "grey.800",
               color: "#fff",
               mb: 4,
               backgroundSize: "cover",
               backgroundRepeat: "no-repeat",
               backgroundPosition: "center",
            }}
         >
            {/* Increase the priority of the hero background image */}
            {<img style={{ display: "none" }} src={producto.image} alt={producto?.title} />}
            <Box
               sx={{
                  position: "absolute",
                  top: 0,
                  bottom: 0,
                  right: 0,
                  left: 0,
                  backgroundColor: "rgba(0,0,0,.3)",
               }}
            />
            <Grid container>
               <Grid item md={6}>
                  <Box
                     sx={{
                        position: "relative",
                        p: { xs: 3, md: 6 },
                        pr: { md: 0 },
                     }}
                  >
                     <Typography component="h1" variant="h3" color="inherit" gutterBottom>
                        {producto.title}
                     </Typography>
                     <Typography variant="h5" color="inherit" paragraph>
                        {producto.description}
                     </Typography>
                  </Box>
               </Grid>
            </Grid>
         </Paper>
      </Container>
   );
};

export default ProductoPage;
