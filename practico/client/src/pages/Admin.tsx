import React, { useEffect, useState } from "react";
import {
   Container,
   Button,
   Table,
   TableBody,
   TableCell,
   TableContainer,
   TableHead,
   TableRow,
} from "@mui/material";
import CrearProductoModal from "../components/CrearProductoModal";
import DeleteIcon from "@mui/icons-material/Delete";
import { history } from "..";
import { useStore } from "../store/store";
import { ProductoInterface, Category } from '../interfaces/Producto';
import apiUrls from "../api/axios-agent";
import { toast } from "react-toastify";
import { observer } from "mobx-react-lite";


const Admin = () => {
   const { modalStore } = useStore();

   const agregarActividad = (actividadAgregada: any) => {
      console.log(actividadAgregada);
   };

   const [productos, setProductos] = useState<ProductoInterface[]>([
      {} as ProductoInterface,
   ]);

   useEffect(() => {
      fetchProducto();
   }, []);

   const fetchProducto = async () => {
      try {
         const response = await apiUrls.Producto.get();
         setProductos(response);
      } catch (error) {
         console.error(error);
      }
   };

   const eliminarProducto = async(idProducto: number) => {
      try {
         await apiUrls.Producto.delete(`${idProducto}`);
         const productoEditado = productos.filter(prod => prod.id !== idProducto)
         setProductos(productoEditado)
      } catch (error) {
         toast.error("Error eliminando producto");
      }
   }

   const actualizarProducto = (producto: ProductoInterface) => {
      setProductos([...productos, producto])
   }

   return (
      <Container sx={{ py: 3 }}>
         <Button
            size="medium"
            variant="contained"
            onClick={() =>
               modalStore.openModal(<CrearProductoModal agregar={actualizarProducto} />)
            }
            sx={{ marginBottom: 3 }}
         >
            Añadir Producto
         </Button>
         <Container>
            <TableContainer>
               <Table sx={{ minWidth: 650 }} aria-label="simple table">
                  <TableHead>
                     <TableRow>
                        <TableCell>Id</TableCell>
                        <TableCell align="right">Title</TableCell>
                        <TableCell align="right">Description</TableCell>
                        <TableCell align="right">Price</TableCell>
                        <TableCell align="right">Category</TableCell>
                        <TableCell align="right">Fecha de Creacion</TableCell>
                        <TableCell align="right"></TableCell>
                     </TableRow>
                  </TableHead>
                  <TableBody>
                     {productos.map((producto) => (
                        <TableRow
                           key={producto.id}
                           sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                        >
                           <TableCell component="th" scope="row">
                              {producto.id}
                           </TableCell>
                           <TableCell align="right">{producto.title}</TableCell>
                           <TableCell align="right">{producto.description}</TableCell>
                           <TableCell align="right">{producto.price}</TableCell>
                           <TableCell align="right">{producto.category}</TableCell>
                           <TableCell align="right">{producto.fechaCreacion}</TableCell>
                           <TableCell align="right">
                              <Button onClick={() => eliminarProducto(producto.id)}>
                                 <DeleteIcon />
                              </Button>
                           </TableCell>
                        </TableRow>
                     ))}
                  </TableBody>
               </Table>
            </TableContainer>
         </Container>
      </Container>
   );
};

export default observer(Admin);
