import { Container, Grid } from '@mui/material'
import React from 'react'
import CartCard from '../components/cards/CartCard'
import { useStore } from '../store/store'

const CartPage = () => {
    const { cartStore } = useStore()

  return (
    <Container sx={{ py: 3 }} maxWidth="xl">
         <Grid container spacing={2}>
            {cartStore.getCart.map((producto) => (
               <CartCard key={producto.id} producto={producto}/>
            ))}
         </Grid>
      </Container>
  )
}

export default CartPage