import React, { useEffect, useState } from "react";
import apiUrls from "../api/axios-agent";
import {
   Container,
   Grid,

} from "@mui/material";
import ProductoCard from "../components/cards/ProductoCard";
import { useStore } from "../store/store";
import { observer } from "mobx-react-lite";

const Homepage = () => {
   const { modalStore  } = useStore();
   const [productos, setProductos] = useState<any[]>([] as any);

   useEffect(() => {
      fetchProductos();
   }, []);

   const fetchProductos = async () => {
      const productosApi = await apiUrls.Producto.get();
      setProductos(productosApi);
   };

   return (
      <Container sx={{ py: 3 }} maxWidth="xl">
         <Grid container spacing={2}>
            {productos.map((producto) => (
               <ProductoCard key={producto.id} producto={producto}/>
            ))}
         </Grid>
      </Container>
   );
};

export default observer(Homepage);
