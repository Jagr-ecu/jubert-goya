import {
   AppBar,
   Button,
   InputBase,
   Menu,
   MenuItem,
   Toolbar,
   Typography,
} from "@mui/material";
import { styled, alpha } from "@mui/material/styles";
import React from "react";
import { Link, NavLink } from "react-router-dom";
import { useStore } from "../store/store";
import SearchIcon from "@mui/icons-material/Search";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";

const Search = styled("div")(({ theme }) => ({
   position: "relative",
   borderRadius: theme.shape.borderRadius,
   backgroundColor: alpha(theme.palette.common.white, 0.15),
   "&:hover": {
      backgroundColor: alpha(theme.palette.common.white, 0.25),
   },
   marginLeft: 0,
   width: "100%",
   [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(1),
      width: "auto",
   },
}));

const SearchIconWrapper = styled("div")(({ theme }) => ({
   padding: theme.spacing(0, 2),
   height: "100%",
   position: "absolute",
   pointerEvents: "none",
   display: "flex",
   alignItems: "center",
   justifyContent: "center",
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
   color: "inherit",
   "& .MuiInputBase-input": {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)})`,
      transition: theme.transitions.create("width"),
      width: "100%",
      [theme.breakpoints.up("sm")]: {
         width: "12ch",
         "&:focus": {
            width: "20ch",
         },
      },
   },
}));

const NavBar = () => {
   const { authStore } = useStore();
   const { usuarioActual: usuario, logout } = authStore;

   const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
   const open = Boolean(anchorEl);
   const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
      setAnchorEl(event.currentTarget);
   };
   const handleClose = () => {
      setAnchorEl(null);
      logout();
   };

   return (
      <AppBar
         position="static"
         color="default"
         elevation={0}
         sx={{ borderBottom: (theme) => `1px solid ${theme.palette.divider}` }}
      >
         <Toolbar sx={{ flexWrap: "wrap" }}>
            <Typography variant="h6" color="inherit" noWrap sx={{ flexGrow: 1 }}>
               <NavLink to="/homepage" style={{ textDecoration: "none", color: "unset" }}>
                  Productos
               </NavLink>
            </Typography>
            {!usuario?.usuario.isAdmin && (
               <Search>
                  <SearchIconWrapper>
                     <SearchIcon />
                  </SearchIconWrapper>
                  <StyledInputBase
                     placeholder="Search…"
                     inputProps={{ "aria-label": "search" }}
                  />
               </Search>
            )}
            {!usuario ? (
               <Link to="/login" style={{ textDecoration: "none" }}>
                  <Button variant="contained" sx={{ my: 1, mx: 1.5 }}>
                     Iniciar Sesión
                  </Button>
               </Link>
            ) : (
               <>
                  <Button
                     id="basic-button"
                     aria-controls={open ? "basic-menu" : undefined}
                     aria-haspopup="true"
                     aria-expanded={open ? "true" : undefined}
                     onClick={handleClick}
                  >
                     {usuario.usuario.username}
                  </Button>
                  <Menu
                     id="basic-menu"
                     anchorEl={anchorEl}
                     open={open}
                     onClose={handleClose}
                     MenuListProps={{
                        "aria-labelledby": "basic-button",
                     }}
                  >
                     <MenuItem onClick={handleClose}>Logout</MenuItem>
                  </Menu>
               </>
            )}
            <Link to="/cart" style={{ textDecoration: "none" }}>
               <Button variant="contained" sx={{ my: 1, mx: 1.5 }}>
                  <ShoppingCartIcon />
               </Button>
            </Link>
         </Toolbar>
      </AppBar>
   );
};

export default NavBar;
