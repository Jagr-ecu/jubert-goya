import React from "react";
import {
   Button,
   Card,
   CardActions,
   CardContent,
   CardMedia,
   Grid,
   Typography,
} from "@mui/material";
import { observer } from "mobx-react-lite";
import { ProductoInterface } from "../../interfaces/Producto";
import { history } from "../..";
import { useStore } from '../../store/store';

interface Props {
   producto: ProductoInterface;
}

const ProductoCard = ({ producto }: Props) => {
   const { cartStore } = useStore()

   return (
      <Grid item xs={3}>
         <Card sx={{ height: "100%", display: "flex", flexDirection: "column" }}>
            <CardMedia
               component="img"
               sx={{
                  // 16:9
                  pt: "56.25%",
                  objectFit: "contain",
                  height: "200px",
                  marginTop: "-120px",
                  marginBottom: '20px'
               }}
               image={producto.image || "./../../../public/favicon.ico"}
               alt="random"
            />
            <CardContent sx={{ flexGrow: 1 }}>
               <Typography gutterBottom variant="h5" component="h2">
                  {producto.title}
               </Typography>
               <Typography>{producto.description}</Typography>
            </CardContent>
            <CardActions>
               <Button size="small" onClick={() => cartStore.añadirItem({...producto, cantidad: 0})}>
                  Añadir
               </Button>
               <Button size="small" onClick={() => history.push(`/producto/${producto.id}`)}>
                  Detalles
               </Button>
            </CardActions>
         </Card>
      </Grid>
   );
};

export default observer(ProductoCard);
