import { MenuItem, TextField } from "@mui/material";
import { useField } from "formik";
import React from "react";

interface Props {
   label: string;
   name: string;
   value?: string;
   onChange?: any;
   selectValues: any[];
   type?: string;
   defaultValue?: any;
}

const SelectInput = (props: Props) => {
    const { name, selectValues } = props
   const [field, meta] = useField(name);

   return (
      <TextField
         margin="normal"
         required
         select
         fullWidth
         autoFocus
         error={meta.touched && !!meta.error}
         helperText={meta.touched && meta.error}
         {...props}
         {...field}
      >
         <MenuItem key={""} value={""}>
            No Seleccionado
         </MenuItem>
         {selectValues.map((option) => (
            <MenuItem key={option.id} value={option.id}>
               {option}
            </MenuItem>
         ))}
      </TextField>
   );
};

export default SelectInput;
