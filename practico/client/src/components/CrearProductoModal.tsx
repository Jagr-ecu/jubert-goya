import { Box, Button, FormControl, MenuItem, Typography } from "@mui/material";
import React from "react";
import { Field, Formik } from "formik";
import * as Yup from "yup";
import TextInput from "./inputs/TextInput";
import { observer } from "mobx-react-lite";
import SelectInput from "./inputs/SelectInput";
import { Select } from "formik-mui";
import { useStore } from '../store/store';
import apiUrls from "../api/axios-agent";

const categorias = [
   { nombre: "electronics" },
   { nombre: "jewelery" },
   { nombre: "men's clothing" },
   { nombre: "women's clothing" },
];

const CrearProductoModal = ({ agregar }: { agregar: any }) => {
   const { modalStore } = useStore()

   const crearActividadLocalmente = (actividad: any) => {
      agregar(actividad);
   };

   return (
      <>
         <Typography id="modal-modal-title" variant="h6" component="h2">
            Agregar Tarea
         </Typography>
         <Formik
            initialValues={{
               title: "",
               description: "",
               price: 0,
               category: "",
            }}
            onSubmit={async (values) => {
               const productoCreado = await apiUrls.Producto.crear(values);
               console.log(values);
               
               crearActividadLocalmente(productoCreado)
               modalStore.closeModal();
            }}
            validationSchema={Yup.object({
               title: Yup.string().required("Se requiere un nombre "),
               description: Yup.string().required("Se requiere una descripcion"),
               precio: Yup.number().required("Se requiere un precio"),
               category: Yup.string().required("Se requiere una categoria"),
            })}
         >
            {({ handleSubmit, isSubmitting, isValid, dirty }) => (
               <form onSubmit={handleSubmit} autoComplete="off">
                  <Box component="main" sx={{ mt: 5 }}>
                     <TextInput label="Nombre de Producto" name="title" />
                     <TextInput label="Precio" name="precio" />
                     <TextInput label="Descripcion" name="description" />
                     <FormControl sx={{minWidth: '100%'}}>
                        <Field
                           style={{ width: '100%', paddingTop: '2px' }}
                           component={Select}
                           type="text"
                           label="Categorias"
                           name="category"
                           inputProps={categorias[0]}
                        >
                           {categorias.map((option) => (
                              <MenuItem key={option.nombre} value={option.nombre}>
                                 {option.nombre}
                              </MenuItem>
                           ))}
                        </Field>
                     </FormControl>
                     <Button
                        type="submit"
                        disabled={!isValid || !dirty || isSubmitting}
                        fullWidth
                        variant="contained"
                        sx={{ mt: 3, mb: 2 }}
                     >
                        Crear Tarea
                     </Button>
                  </Box>
               </form>
            )}
         </Formik>
      </>
   );
};

export default observer(CrearProductoModal);
