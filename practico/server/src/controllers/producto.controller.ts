import { NoAutorizadoError } from './../errors/no-autorizado-error';
import { Request, Response, NextFunction } from "express";
import ProductoService from "../services/producto.service";
import { UsuarioPayload } from '../middleware/validar-jwt';

declare global {
   namespace Express {
       interface Request {
           usuario?: UsuarioPayload;
       }
   }
} 


const crearProducto = async (
    req: Request,
    res: Response,
    next: NextFunction
 ) => {
    try {
      if (!req.usuario?.isAdmin) throw new NoAutorizadoError();

       const respuesta = await ProductoService.crearProducto(req.body as any);
       res.status(200).json(respuesta);
    } catch (error) {
       next(error)
    }
};

const eliminarProducto = async (
    req: Request,
    res: Response,
    next: NextFunction
 ) => {
   if (!req.usuario?.isAdmin) throw new NoAutorizadoError();

    try {
       const respuesta = await ProductoService.eliminarProducto(req.params as any);
       res.status(200).json(respuesta);
    } catch (error) {
       next(error);
    }
};

const getProductos = async (
    req: Request,
    res: Response,
    next: NextFunction
 ) => {
    try {
       const respuesta = await ProductoService.obtenerProductos(req.query as any);
       res.status(200).json(respuesta);
    } catch (error) {
       next(error);
    }
};

const getProductoById = async (
    req: Request,
    res: Response,
    next: NextFunction
 ) => {
    try {
       const respuesta = await ProductoService.buscarPorProducto(req.params as any);
       res.status(200).json(respuesta);
    } catch (error) {
       next(error);
    }
};

export {
    crearProducto,
    eliminarProducto,
    getProductos,
    getProductoById
}