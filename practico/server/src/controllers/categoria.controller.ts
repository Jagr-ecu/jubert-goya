import { Request, Response, NextFunction } from "express";
import CategoriaService from '../services/categoria.service';

export const getCategorias = async (
    req: Request,
    res: Response,
    next: NextFunction
 ) => {
    try {
       const respuesta = await CategoriaService.obtenerCategorias();
       res.status(200).json(respuesta);
    } catch (error) {
       next(error);
    }
};