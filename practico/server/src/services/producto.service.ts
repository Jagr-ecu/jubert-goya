import { SolicitudIncorrectaError } from "../errors/solicitud-incorrecta-error";
import { Producto } from "../models/producto.model";

export default class ProductoService {
   static async crearProducto(body: any) {
      const { title, description, price, category, image } = body;

      const producto = Producto.build({ title, description, price, category, image });
      await producto.save();

      return producto;
   }

   static async obtenerProductosPorId(params: any) {
    const { id } = params;

    const producto = await Producto.findOne({ state: true, _id: id });
    if (!producto) throw new SolicitudIncorrectaError("No existe producto");

    return producto;
 }

   static async buscarPorProducto(params: any) {
      const { id } = params;

      const producto = await Producto.findOne({ _id: id });
      if (!producto) throw new SolicitudIncorrectaError("No existe producto");

      return producto;
   }

   static async eliminarProducto(params: any) {
      const { id } = params;

      const producto = await Producto.findByIdAndUpdate(id , { state: false });

      if (!producto) throw new SolicitudIncorrectaError("No existe producto");

      return { msg: 'producto borrado' };
   }

   static async obtenerProductos(bodyQuery: any) {
    const { title } = bodyQuery;
    
    const query = title ? { state: true, title: { $regex: title } } : { state: true }

    const producto = await Producto.find(query);
    if (!producto) throw new SolicitudIncorrectaError("No existe producto");

    return producto;
 }
}
