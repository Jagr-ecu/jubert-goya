import { SolicitudIncorrectaError } from "../errors/solicitud-incorrecta-error";
import { Categoria } from "../models/categoria.model";

export default class CategoriaService {
   static async obtenerCategorias() {
      const categoria = await Categoria.find();
      if (!categoria) throw new SolicitudIncorrectaError("No existe categoria");

      return categoria;
   }
}
