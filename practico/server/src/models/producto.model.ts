import mongoose, { Schema } from 'mongoose';
import { PasswordManager } from '../services/password-manager';
import { Category } from './interfaces/Producto';

interface ProductoAtributos {
    title:       string;
    price:       number;
    description: string;
    category:    string;
    image?:       string;
}

interface ProductoModel extends mongoose.Model<ProductoDoc> {
    build(attrs: ProductoAtributos): ProductoDoc;
}

export interface ProductoDoc extends mongoose.Document {
    title:       string;
    price:       number;
    description: string;
    category:    string;
    image:       string;
    state:  string;
} 


const ProductoSchema = new mongoose.Schema({
    title: {
        type: String,
        required: [true, 'El nombre de la actividad es obligatoria']
    },
    price: {
        type: Number,
        required: [true, 'Se requiere la fecha fin']
    },
    description: {
        type: String,
        required: [true, 'Se requiere la fecha fin']
    },
    category: {
        type: String,
        default: Category.Electronics,
        emun: Category,
    },
    image: {
        type: String,
        default: null
    },
    state: {
        type: Boolean,
        default: true
    },
    fechaCreacion: {
        type: Date,
        default: () => Date.now(),
    }
}, {
    toJSON: {
        transform(doc, ret) {
            ret.id = ret._id;
            delete ret._id;
            delete ret.__v;
        }
    }
});

ProductoSchema.statics.build = (attrs: ProductoAtributos) => {
    return new Producto(attrs);
}

const Producto = mongoose.model<ProductoDoc, ProductoModel>('Producto', ProductoSchema);

export { Producto };