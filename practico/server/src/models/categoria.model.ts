import mongoose, { Schema } from 'mongoose';
import { PasswordManager } from '../services/password-manager';
import { Category } from './interfaces/Producto';

interface CategoriaAtributos {
    nombre: string
}

interface CategoriaModel extends mongoose.Model<CategoriaDoc> {
    build(attrs: CategoriaAtributos): CategoriaDoc;
}

export interface CategoriaDoc extends mongoose.Document {
    nombre: string
} 


const ProductoSchema = new mongoose.Schema({
    nombre: {
        type: String,
        required: [true, 'El nombre de la categoria es obligatoria']
    },
    state: {
        type: Boolean,
        default: true
    },
    fechaCreacion: {
        type: Date,
        default: () => Date.now(),
    }
}, {
    toJSON: {
        transform(doc, ret) {
            ret.id = ret._id;
            delete ret._id;
            delete ret.__v;
        }
    }
});

ProductoSchema.statics.build = (attrs: CategoriaAtributos) => {
    return new Categoria(attrs);
}

const Categoria = mongoose.model<CategoriaDoc, CategoriaModel>('Categoria', ProductoSchema);

export { Categoria };