import mongoose, { Schema } from 'mongoose';
import { Category } from './interfaces/Producto';

interface OrdenAtributos {
    title:       string;
    price:       number;
    description: string;
    category:    string;
    image?:       string;
}

interface OrdenModel extends mongoose.Model<OrdenDoc> {
    build(attrs: OrdenAtributos): OrdenDoc;
}

export interface OrdenDoc extends mongoose.Document {
    title:       string;
    price:       number;
    description: string;
    category:    string;
    image:       string;
    state:  string;
} 


const OrdenSchema = new mongoose.Schema({
    nombreCliente: {
        type: String,
        required: [true, 'El nombre del cliente es obligatoria']
    },
    clienteDireccion: {
        type: String,
        required: [true, 'La direccion del cliente es obligatoria']
    },
    nombreFacturacion: {
        type: String,
        required: [true, 'El nombre de la factura es obligatoria']
    },
    facturacionDireccion: {
        type: String,
        required: [true, 'La direccion de la facturacion es obligatoria']
    },
    subtotal: {
        type: Number,
        required: [true, 'El subtotal es obligatoria']
    },
    impuestos: {
        type: Number,
        required: [true, 'El impuesto es obligatoria']
    },
    total: {
        type: Number,
        required: [true, 'El total es obligatoria']
    },
    formaPago: {
        type: String,
        default: Category.Electronics,
        emun: Category,
    },
    state: {
        type: Boolean,
        default: true
    },
    fechaCreacion: {
        type: Date,
        default: () => Date.now(),
    }
}, {
    toJSON: {
        transform(doc, ret) {
            ret.id = ret._id;
            delete ret._id;
            delete ret.__v;
        }
    }
});

OrdenSchema.statics.build = (attrs: OrdenAtributos) => {
    return new Orden(attrs);
}

const Orden = mongoose.model<OrdenDoc, OrdenModel>('Orden', OrdenSchema);

export { Orden };