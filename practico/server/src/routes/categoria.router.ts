import { Router } from "express";
import { getCategorias } from "../controllers/categoria.controller";

const categoriaRouter = Router();

//usuarioRouter.post("/admin", crearUsuarioAdmin);
categoriaRouter.get(
   "/",
   getCategorias
);

export default categoriaRouter
