import express from "express";
import categoriaRouter from "./categoria.router";
import productoRouter from "./producto.router";

import usuarioRouter from "./usuario.router";

const api = express.Router();

api.use("/auth", usuarioRouter);
api.use("/productos", productoRouter);
api.use("/categorias", categoriaRouter);

export default api;
