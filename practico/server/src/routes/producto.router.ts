import { Router } from "express";
import { crearProducto, eliminarProducto, getProductoById, getProductos } from "../controllers/producto.controller";
import { validarJWT } from '../middleware/validar-jwt';

const productoRouter = Router();

//usuarioRouter.post("/admin", crearUsuarioAdmin);
productoRouter.post(
   "/",
   validarJWT,
   crearProducto
);
productoRouter.delete('/:id', validarJWT, eliminarProducto);
productoRouter.get('/:id', getProductoById);
productoRouter.get('/', getProductos);

export default productoRouter;