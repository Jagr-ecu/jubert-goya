import { ProductoInterface as ProductoInterface } from "../models/interfaces/Producto";
import mongoose from "mongoose";
import { Usuario } from "../models/usuario.model";
import { PasswordManager } from "../services/password-manager";
import { Producto } from "../models/producto.model";
import fetch from "node-fetch";

const seedData = async () => {
   const contrasenaAdmin = await PasswordManager.toHash("1234");

   let doc = await Usuario.findOneAndUpdate(
      { username: "admin1" },
      { username: "admin1", contrasena: contrasenaAdmin, isAdmin: true },
      {
         upsert: true, // Make this update into an upsert
      }
   );

   const productoCount = await Producto.countDocuments();
   console.log('Cantidad de productos = '+productoCount);

   if (productoCount === 0) {
      const response = await fetch("https://fakestoreapi.com/products");
      const json = (await response.json()) as any;
      let resultData = [...json] as ProductoInterface[];

      for (let i = 0; i < resultData.length; i++) {
         const { title, description, price, category, image } = resultData[i];
         let productoSave = new Producto({ title, description, price, category, image });
         productoSave.save();
      }
   }
};

export { seedData };
